# GatewayGaiax

## The Role of the Gateway Gaia-X component

The role of the Gaia-X Gateway is to make calls to the various components of the Gaia-x ecosystem. We have implemented this architecture so that the ICP component is Generic. In other words, it works with Gaia-X, but it could also work with another ecosystem. This is made possible by the plugin system implemented in the Gateway (a plugin represents a type of VC). 
This is explained in detail in the section [Gateway architecture](#architecture-of-gateway-gaia-x), and also in the section [Add a plugin for a new type of VC to be issued](#add-a-plugin-for-a-new-type-of-vc-to-be-issued), which explains how to easily add a new plugin, i.e. a new type of VC to be issued.

## Architecture of Gateway Gaia-X

The Gateway's architecture has been designed so that new VCs can be easily added to issuer. This is made possible by a system of plugins that are loaded at application startup (one plugin represents one type of VC).

 - Firstly, the Gateway implements a **GET /issuance** route which returns a list of VCs that the Gateway is able to issue. This list is retrieved via the configuration file **src/config/default-vc-configuration.json**. This file contains an array of objects, where each object contains the VC configuration to be issued. In addition, this file can be updated via the **PUT /config/credentials** route.
 Here are the properties of a VC object:
     - **name**: This is the name of the VC.
     - **type**: This is the type of VC issuance (form, presentation, keycloak).
     - **form** (if type is form): This parameter is only present if parameter **type** is **form**. This parameter contains a JSON object representing the form to be displayed to the user. This form must respect the JSONSchema of the [react-jsonschema-form](https://github.com/rjsf-team/react-jsonschema-form) library.
     - **presentation** (if type is presentaion) This parameter is only present if parameter **type** is **presentation**. This parameter contains the verifiable JSON-format definition of the VCs to be shared by the user.
     - **issuer**: This is a table containing the names of providers and/or federations that can issue the VC.
     - **fieldsToExtract**:  This is an object array containing the name of a field and its associated JsonPATH to enable simple extraction of a field in the VC in question. This parameter is optional.

Below is an example of a configuration file:
```json
[
    {
        "name": "RegistrationNumber",
        "type": "presentation",
        "presentation": {
            "format":null,
            "id":"1",
            "input_descriptors":[
                {
                    "constraints": null,
                    "format": null,
                    "group": null,
                    "id": "1",
                    "name": null,
                    "purpose": null,
                    "schema": {
                      "uri": "https://registry.aster-x.demo23.gxfs.fr/api/trusted-schemas-registry/v2/schemas/LegalParticipantSchema"
                    }
                }
            ],
            "name": null,
            "purpose": null,
            "submission_requirements": null
        },
        "fieldsToExtract" : [
            { "name": "leiCode", "jpath": "credentialSubject[gx:leiCode]" },
            { "name": "vatID", "jpath": "credentialSubject[gx:vatID]" },
            { "name": "EORI", "jpath": "credentialSubject[gx:EORI]" }
        ],
        "issuer": ["aster-x", "aster-x", "localhost"]
    },
    {
        "name": "EmployeeCredential",
        "type": "keycloak",
        "fieldsToExtract" : [
            { "name": "name", "jpath": "credentialSubject[name]" },
            { "name": "surname", "jpath": "credentialSubject[surname]" },
            { "name": "email", "jpath": "credentialSubject[email]" }
        ],
        "issuer": ["dufourstorage", "montblanc-iot", "localhost"]
    },
    {
        "name": "TermsAndConditions",
        "type": "form",
        "form": {
            "title": "Sign Gaia-X Terms And Conditions",
            "type": "object",
            "properties": {
              "text": {
                "type": "null",
                "title": "Gaiax",
                "description": "The PARTICIPANT signing the Self-Description agrees the following conditions: First, update its descriptions about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the descriptions. Secondly, the keypair used to sign Verifiable Credentials will be revoked where Gaia-X Association becomes aware of any inaccurate statements in regards to the claims which result in a non-compliance with the Trust Framework and policy rules defined in the Policy Rules and Labelling Document (PRLD)."
              },
              "accept_conditions": {
                "type": "boolean",
                "default": false,
                "description": "By checking this box, you agree to the above terms and conditions.",
                "title": "Accept terms and conditions"
              }
            }
        },
        "fieldsToExtract" : [
        ],
        "issuer": ["dufourstorage", "montblanc-iot", "localhost"]
    }
]
```
 - Then, for each VC in the configuration file, a javascript file with the name {vcName}.js must be placed in the plugins directory. This file must have the same name as the **name** field in the previous configuration file in order to make the connection. This file will also be automatically loaded when the gateway is restarted. This file must contain at least two mandatory functions:
     - **startIssuance** : This function takes no parameters and returns the VC configuration JSON object.
     - **continueIssuance**: This function takes a **data** parameter which can contain a Verifiable Presentation containing the VCs the user has shared, the form data filled in by the user, or a JWT keycloak containing the user's information. Depending on the type of VC. The function then processes the data received as parameters, calling Gaia-X components such as Compliance and RegistratonNumber. Once the data has been processed, the function returns the new VC data to be issued. The return format is a JSON object of the form: ```{ credential: {object}, credential_type: {string}}``` Where **credential** is an object containing the VC data (credentialSubject, evidence, etc.), and **credential_type** is a string containing the VC type.
 
 - Finally, the Gateway implements the **POST /issuance/{vcName}** route, which is used to issue a VC. The {vcName} parameter in the route must also have the same name as the plugin and VC name in the configuration file, as explained above. This route does two things: 
     - Start an issuance: the data returned by this route is that of the **startIssuance** function of the plugin concerned (plugin **{vcName}**). The data returned is therefore, for example, a form or a veriafiable presentation. This route is called by the ICP with this object in body: ```{ action: "startIssuance" }```.
     - ContinueIssuance: The data returned is that of the **continueIssuance** function (which contains the VC data). This route is called by the ICP with this object in body: ```{ action: "continueIssuance", {type}: {object} }``` where the **type** property can have the values **form**, **jwt**, or **presentation**.

## Add a plugin for a new type of VC to be issued
Each type of VC issued by ICP is represented by a plugin in the gateway.
The steps below describe the installation and configuration of a new VC type:
  - In the gateway's VC configuration file, add a new object representing the new VC to be issued:
  Below is an example for the **MyVC** VC with the associated VerifiableDefinition **MyVerifiableDefinition** and issuers
  ```json
  {
        "name": "MyVC",
        "type": "presentation",
        "presentation": { "MyVerifiableDefinition" },
        "issuer": ["aster-x", "aster-x", "localhost"]
  },
  ```
  - Create a javascript file (extension **.js**) with the same name as the **name** parameter above, followed by the **.js** extension : **MyVC.js**
  - In this file, create the :
    - **startIssuance** (See [Gateway Gaia-X](#gateway-gaia-x) for more details)
    - **continueIssuance** (See [Gateway Gaia-X](#gateway-gaia-x) for more details)
  - Restart the Gaia-X Gateway components, and your ICP will be able to deliver the new VC.

# Technologies and tools used

 - Node.js
 - ReactJS
 - OID4VCI, OID4VP, and Status List v2021 protocols
 - Keycloak
 - Mongodb
 - Wallet (Walt.id, Talao, Sphereon)

# Installation

To install ICP locally, the  components Portal, AdminPortal, CredentialIssuer, and Gaia-x Gateway must be running simultaneously. The links can be found below:
- [Portal](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/portal)
- [CredentialIssuer](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer)
- [Gateway Gaia-x](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/gatewaygaiax)
- [AdminPortal](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/adminportal)

To run the components : `npm install` and `npm start`

# Authors and acknowledgment
GXFS-FR

# License
The ICP is delivered under the terms of the Apache License Version 2.0.