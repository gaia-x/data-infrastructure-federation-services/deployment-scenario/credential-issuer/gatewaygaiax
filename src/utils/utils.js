const zlib = require('zlib');

exports.getCompressedData = async (data) => {
    return new Promise((resolve, reject) => {
      zlib.gzip(data, (error, compressedData) => {
        if (error) {
          reject(error);
        } else {
          resolve(compressedData);
        }
      });
    });
}
exports.isEmployee = (employeeArray, company, email) => {
    if (Array.isArray(employeeArray) && employeeArray.length > 0) {
        const employee = employeeArray.find(e => e.company === company && e.email === email);  // Vérifie si le nom et l'email correspondent
        return employee ? employee.adresseWalletCompany : ''; // Renvoie l'adresse du wallet corporate si une correspondance est trouvée
    } else {
        return '';
    }
};

exports.getTypeValues=(value) =>{
    if (typeof value === 'string') {
        // Si la valeur est une chaîne de caractères, on la renvoie.
        return value;
    } else if (Array.isArray(value)) {
        // Si la valeur est un tableau, on parcourt chaque élément du tableau.
        return value.flatMap((item) => {
            if (typeof item === 'object' && item !== null && item.hasOwnProperty('type')) {
                // Si l'élément est un objet et a une propriété 'type', on ajoute la valeur de cette propriété au tableau.
                return item.type;
            } else {
                // Sinon, on renvoie un tableau vide.
                return '';
            }
        });
    } else {
        // Si la valeur n'est ni une chaîne de caractères ni un tableau, on renvoie un tableau vide.
        return '';
    }
}

