const cors = require('cors');
const express = require('express');
const compression = require('compression')
const logger = require('./services/logger');
const plugin = require('./services/plugins');
const HttpError = require('./utils/httpError');
const issuanceController = require('./controllers/issuanceController');
const configurationController = require('./controllers/configurationController');
const pluginController = require('./controllers/pluginController');

/* For local development */
if (process.env.NODE_ENV !== "production") require("dotenv").config();

/* Express Configuration */
const app = express();
app.use(compression())
const PORT = 3003; 
app.use(express.json());
app.use(cors());

/* Database creation */
const { init } = require("./services/mongodb");
(async ()=> {
    await init();
})();

/* Middleware: API Key verification */
function verifyApiKey(req, res, next) {  
  if (req.headers['x-api-key'] && req.headers['x-api-key'] === process.env.API_KEY_AUTHORIZED) {
    next();
  } else {
    res.status(401).json({ error: 'Unauthorised: wrong api key' });
  }
}

/* Issuance Route */
app.get('/issuance', issuanceController.listOfIssuableVCs);
app.put('/issuance', verifyApiKey, configurationController.putCredentials);
app.post('/issuance/:vcType', issuanceController.issuance);

/* Configuration Route for OIDC */
app.get('/config/credentials_supported', configurationController.getCredentialsSupported);
app.put('/config/credentials_supported', verifyApiKey, configurationController.putCredentialsSupported);
app.get('/presentation-definitions', configurationController.getAllPresentationDefinitions); // To get All presentation Definitions
app.get('/presentation-definitions/:name', configurationController.getPresentationDefinition);

/* Plugin Client Side Template */
app.get('/legal-participant.js', pluginController.pluginClientForm);
app.get('/legal-participant-select.css', pluginController.pluginClientFormSelectCss);
app.get('/legal-participant-select.js', pluginController.pluginClientFormSelectJs);

/* Error */
app.use((error, req, res, next) => {
  logger.error(error.message, { stackTrace: error.stack ? error.stack : 'No stack trace' });
  if (error instanceof HttpError && error.statusCode !== 500) {
    res.status(error.statusCode).json({ error: error.message });
  } else {
    res.status(500).json({ error: 'Internal server error' });
  }
});

/* Start the server */
app.listen(PORT, () => {
  plugin.pluginsLoad(app);
  logger.info(`Server listening on port ${PORT}`);
});