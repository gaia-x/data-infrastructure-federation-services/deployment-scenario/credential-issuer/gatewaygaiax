const HttpError = require("../utils/httpError");
const { getOne } = require('../services/mongodb');

const startIssuance = async (data) => {
    try {
        let response;
        const vcConfiguration = await getOne('Configuration', 'VCsConfiguration');
        for (const credential of vcConfiguration) {
            if (credential.name === "EudiPid") {
                const credentialType = credential.types.find((item) => item.type === data.typeOfIssuance);
                return { type: credentialType.type, form: credentialType.form }
            }
        }
        return response;
    }
    catch (error) {
        throw new HttpError(`Error to get the configuration for credential EuPid: ${error.message}`, 400);
    }
}

const continueIssuance = async (data) => {
    try {
        const vcConfiguration = await getOne('Configuration', 'VCsConfiguration');

        let vct;
        let credentialType;
        let sdVCClaimsDisclosureFrame;
        for (const credential of vcConfiguration) {
            if (credential.name === "EudiPid") {
                vct = credential.vct;
                credentialType = credential.name;
                sdVCClaimsDisclosureFrame = credential.sdVCClaimsDisclosureFrame;
            }
        }

        const credential = {
            vct: vct,
            vcClaims: data.form,
            sdVCClaimsDisclosureFrame: sdVCClaimsDisclosureFrame
        }
        credential.vcClaims.issuing_country = "FR";
        credential.vcClaims.issuing_authority = "FR";

        return { credentials_information: [{ format: 'vc+sd-jwt', type: credentialType, credential: credential}]}
    }
    catch (error) {
        throw error instanceof HttpError ? error : new HttpError(`Error in continueIssuance for EuPid: ${error.message}`, 500);
    }
}

module.exports = {
    startIssuance,
    continueIssuance
}