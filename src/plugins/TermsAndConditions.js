const HttpError = require("../utils/httpError");
const { getOne } = require('../services/mongodb');

const startIssuance = async (data) => {
    try {
        let response;
        const vcConfiguration = await getOne('Configuration', 'VCsConfiguration');
        for (const credential of vcConfiguration) {
            if (credential.name === "TermsAndConditions") {
                const credentialType = credential.types.find((item) => item.type === data.typeOfIssuance);
                return { type: credentialType.type, form: credentialType.form }
            }
        }
        return response;
    }
    catch (error) {
        throw new HttpError(`Error to get the configuration for credential TermsAndConditions: ${error.message}`, 400);
    }
}

const continueIssuance = (data) => {
    const credentialType = "gx:GaiaXTermsAndConditions";
    const credential = {
        credentialSubject: {
            "gx:termsAndConditions": "The PARTICIPANT signing the Self-Description agrees the following conditions: First, update its descriptions about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the descriptions. Secondly, the keypair used to sign Verifiable Credentials will be revoked where Gaia-X Association becomes aware of any inaccurate statements in regards to the claims which result in a non-compliance with the Trust Framework and policy rules defined in the Policy Rules and Labelling Document (PRLD).",
            "type": credentialType
        }
    }
    return { credentials_information: [{ format: 'ldp_vc', type: credentialType, credential: credential }]};
}

module.exports = {
    startIssuance,
    continueIssuance
}