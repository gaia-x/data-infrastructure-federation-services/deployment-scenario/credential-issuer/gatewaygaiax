const HttpError = require("../utils/httpError");
const { getOne } = require('../services/mongodb');

const startIssuance = async (data) => {
    try {
        let response;
        const vcConfiguration = await getOne('Configuration', 'VCsConfiguration');
        for (const credential of vcConfiguration) {
            if (credential.name === "LegalParticipant") {
                const credentialType = credential.types.find((item) => item.type === data.typeOfIssuance);
                return { type: credentialType.type, form: credentialType.form }
            }
        }
        return response;
    }
    catch (error) {
        throw new HttpError(`Error to get the configuration for credential LegalParticipant: ${error.message}`, 400);
    }
}

const continueIssuance = (data) => {
    const credentialType = "gx:LegalParticipant";
    // TODO: Verify the data
    let credential = {
        credentialSubject: {
            type: credentialType,
            ...data.form
        }
    };
    return { credentials_information: [{ format: 'ldp_vc', type: credentialType, credential: credential }]};
}

module.exports = {
    startIssuance,
    continueIssuance
}