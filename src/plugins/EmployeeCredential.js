const jwt = require('jsonwebtoken');
const { v4: uuidv4 } = require('uuid');
const HttpError = require("../utils/httpError");
const keycloak = require('../services/keycloak');

const startIssuance = () => {
    return { type: 'keycloak', redirectUri: keycloak.getKeycloakLoginUrl() }
}

const continueIssuance = (data) => {
    try {
        switch (data.type) {
            case 'keycloakJWT': {
                const decodedJWT = jwt.decode(data.keycloakJWT.proof.jwt);
                const credentialType = "EmployeeCredential";
                const credential = {
                    "@context": [
                        "https://www.w3.org/2018/credentials/v1",
                        {
                            "EmployeeCredential": {
                                "@id": "https://example.com/context#employeecredential",
                                "@context": {
                                    "@version": 1.1,
                                    "@protected": true,
                                    "schema": "https://schema.org/",
                                    "id": "@id",
                                    "type": "@type",
                                    "email": "schema:email",
                                    "firstName": "schema:firstName",
                                    "familyName": "schema:familyName",
                                    "phoneNumber": "schema:phoneNumber",
                                    "company": "schema:company"
                                }
                            },
                            "StatusList2021Entry": {
                                "@id": "https://w3id.org/vc/status-list#StatusList2021Entry",
                                "@context": {
                                    "@protected": true,
                                    "id": "@id",
                                    "type": "@type",
                                    "statusPurpose": "https://w3id.org/vc/status-list#statusPurpose",
                                    "statusListIndex": "https://w3id.org/vc/status-list#statusListIndex",
                                    "statusListCredential": {
                                        "@id": "https://w3id.org/vc/status-list#statusListCredential",
                                        "@type": "@id"
                                    }
                                }
                            }
                        }
                    ],
                    type: ["VerifiableCredential", credentialType],
                    id: `https://${decodedJWT.company}.example/${uuidv4()}`,
                    credentialSubject: {
                        id: `https://${decodedJWT.company}.example/users/${decodedJWT.sid}`,
                        type: credentialType,
                        email: decodedJWT.email,
                        firstName: decodedJWT.given_name,
                        familyName: decodedJWT.family_name,
                        phoneNumber: decodedJWT.phone_number,
                        company: decodedJWT.company
                    }
                }
                return { credentials_information: [{ format: 'ldp_vc', type: credentialType, credential: credential }]};
            }
            default:
                throw new HttpError(`Issuance type of VC EmployeeCredential not supported: ${data.type}`, 400);
        }
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError(`Error in continueIssuance for EmployeeCredential: ${error.message}`, 500));
    }
}

module.exports = {
    startIssuance,
    continueIssuance
}