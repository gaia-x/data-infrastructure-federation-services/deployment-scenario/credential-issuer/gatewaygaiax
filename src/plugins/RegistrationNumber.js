const axios = require('axios');
const HttpError = require("../utils/httpError");
const { getOne } = require('../services/mongodb');
const routingTable = require('../services/routingTableManager');

const startIssuance = async (data) => {
    try {
        let response;
        const vcConfiguration = await getOne('Configuration', 'VCsConfiguration');
        for (const credential of vcConfiguration) {
            if (credential.name === "RegistrationNumber") {
                const credentialType = credential.types.find((item) => item.type === data.typeOfIssuance);
                return { type: credentialType.type, presentation: credentialType.presentation }
            }
        }
        return response;
    }
    catch (error) {
        throw new HttpError(`Error to get the configuration for credential RegistrationNumber: ${error.message}`, 400);
    }
}

const continueIssuance = async (data) => {
    try {
        console.log(data)
        const credentialType = "gx:legalRegistrationNumber";

        const vc = data.vp.verifiableCredential[0];
        let resgistrationNumber = vc.credentialSubject["gx:legalRegistrationNumber"];
        const rgCData = {
            "@context": [
                "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant"
            ],
            "type": "gx:legalRegistrationNumber",
            "id": vc.id,
        };

        if (resgistrationNumber['gx:vatID']) rgCData['gx:vatID'] = resgistrationNumber['gx:vatID'];
        if (resgistrationNumber['gx:EORI']) rgCData['gx:EORI'] = resgistrationNumber['gx:EORI'];
        if (resgistrationNumber['gx:leiCode']) rgCData['gx:leiCode'] = resgistrationNumber['gx:leiCode'];

        const url = `${routingTable.getConnectionUrl('registration', 'registrationNumber')}?vcid=${vc.id}`;
        let responseRegistration = await axios.post(url, rgCData);
        if (responseRegistration.status === 200) {
            const credential = {
                credentialSubject: responseRegistration.data.credentialSubject,
                evidence: responseRegistration.data.evidence
            }
            credential.credentialSubject.type = credentialType;
            return { credentials_information: [{ format: 'ldp_vc', type: credentialType, credential: credential }]};
        }
    } catch (error) {
        throw (error instanceof HttpError ? error : new HttpError(`Error in continueIssuance for RegistrationNumber: ${error.message}`, 500));
    }
}

module.exports = {
    startIssuance,
    continueIssuance
}