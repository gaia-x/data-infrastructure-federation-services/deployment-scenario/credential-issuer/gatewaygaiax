const jwt = require('jsonwebtoken');
const utils = require('../utils/utils');
const HttpError = require("../utils/httpError");
const keycloak = require('../services/keycloak');
const { getOne } = require('../services/mongodb');
const listEmployee = require("../config/employee/employee.json");

const startIssuance = async (data) => {
    try {
        let response;
        const vcConfiguration = await getOne('Configuration', 'VCsConfiguration');
        for (const credential of vcConfiguration) {
            if (credential.name === "DelegationCredential") {
                const credentialType = credential.types.find((item) => item.type === data.typeOfIssuance);
                if (data.typeOfIssuance === "keycloak") {
                    return { type: 'keycloak', redirectUri: keycloak.getKeycloakLoginUrl() }
                }
                else if (data.typeOfIssuance === "presentation") {
                    return { type: credentialType.type, presentation: credentialType.presentation };
                }
                else throw new HttpError(`Invalid typeOfIssuance: ${data.typeOfIssuance}`, 400);
            }
        }
        return response;
    }
    catch (error) {
        throw new HttpError(`Error to get the configuration for credential DelegationCredential: ${error.message}`, 400);
    }
}
const continueIssuance = (data) => {
    try {
        if (data.vp) {
            const credentialType = "gx:DelegationCredential";
            const vc = data.vp.verifiableCredential[0];
            const company = vc.credentialSubject["company"];
            const email = vc.credentialSubject["email"];
            const adresseWallet = utils.isEmployee(listEmployee.Listes, company, email);

            if (adresseWallet !== "") {
                const credential = {
                    credentialSubject: {
                        'id': vc.id,
                        'type': credentialType,
                        'gx:email': vc.credentialSubject["email"],
                        'gx:firstName': vc.credentialSubject["firstName"],
                        'gx:familyName': vc.credentialSubject["familyName"],
                        'gx:phoneNumber': vc.credentialSubject["phoneNumber"],
                        'gx:company': vc.credentialSubject["company"],
                        'gx:adresseWalletCompany': adresseWallet
                    }
                }
                return { credential: credential, credential_type: credentialType, format: "ldp_vc" };
            } else {
                throw new HttpError(`This employee isn't member of the organisation`, 401);
            }
        } else {
            const decodedJWT = jwt.decode(data.jwt.proof.jwt);
            const credentialType = "gx:DelegationCredential";
            const credential = {
                credentialSubject: {
                    'id': decodedJWT.sid,
                    'type': credentialType,
                    'gx:email': decodedJWT.email,
                    'gx:firstName': decodedJWT.given_name,
                    'gx:familyName': decodedJWT.family_name,
                    'gx:phoneNumber': decodedJWT.phone_number,
                    'gx:company': decodedJWT.company,
                    'gx:adresseWalletCompany': decodedJWT.adresse_wallet_company
                }
            }
            return { credential: credential, credential_type: credentialType, format: "ldp_vc" }
        }
    }
    catch (error) {
        throw error instanceof HttpError ? error : new HttpError(`Error in continueIssuance for DelegationCredential: ${error.message}`, 500);
    }
};


module.exports = {
    startIssuance,
    continueIssuance,
}