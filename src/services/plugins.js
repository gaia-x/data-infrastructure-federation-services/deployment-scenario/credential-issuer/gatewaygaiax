const fs = require('fs');
const path = require('path');
const logger = require('./logger');

global.PLUGINS = new Map();

const pluginDirectory = path.join(__dirname, "../plugins/");

// Load all plugins from the plugin directory
let pluginsLoad = function (app) {
  return new Promise((result, error) => {
    try {
      fs.readdirSync(pluginDirectory).forEach(file => {
        if (path.extname(file) == ".js") {
          let plugin = require(path.join(pluginDirectory, file));
          pluginsAdd(path.basename(file, '.js'), plugin);
          logger.info(`Load plugin ${file}`);
          result(true);
        }
      });
    } catch (error) { 
      logger.error('Une erreur s\'est produite lors de la chargement des plugins:', error); 
      process.exit(1);
    }
  });
}

// Add a plugin to the map
let pluginsAdd = function(codeEvent, pointeurFonction) {
  if (!global.PLUGINS.has(codeEvent)) { global.PLUGINS.set(codeEvent, new Array()) };
  global.PLUGINS.get(codeEvent).push(pointeurFonction);
}

// Execute a plugin
let pluginsExecute = function(service, codeEvent, input, request) {
  return new Promise((result, error) => {
    if (global.PLUGINS.has(service)) {
      try {
        const functionToExecute = global.PLUGINS.get(service)[0][codeEvent];
        return result(functionToExecute(input, request));
      }
      catch (err) { return error (err); }
    }
    else result(input);
})}

module.exports = { pluginsExecute, pluginsLoad };