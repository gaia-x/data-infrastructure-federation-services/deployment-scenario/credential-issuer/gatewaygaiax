const fs = require('fs');
const path = require('path');

const routingTableFilePath = path.join(__dirname, '..', 'config', 'routingTable.json');
const routingTable = JSON.parse(fs.readFileSync(routingTableFilePath, 'utf8'));

exports.getConnectionUrl = (serviceId, endpoint) => {
    const service = routingTable.services.find((s) => s.id === serviceId);
    if (!service) {
        throw new Error(`Service "${serviceId}" does not exist.`);
    }

    const apiEndpoint = service.api[endpoint];
    if (!apiEndpoint) {
        throw new Error(`Endpoint "${endpoint}" does not exist for service "${serviceId}".`);
    }

    return apiEndpoint;
};