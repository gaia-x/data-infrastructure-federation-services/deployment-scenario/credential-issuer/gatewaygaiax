const logger = require('./logger');
const { MongoClient } = require('mongodb');
const defaultVCsConfiguration = require('../config/default-vc-configuration.json');
const defaultCredentialsSupported = require('../config/default-credentials-supported.json');
const defaultPresentationDefinitions = require('../config/default-presentation-definitions.json');

let client;
let config = {
    "connectionString" : `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@${process.env.MONGO_SERVICE_HOST}:${process.env.MONGO_SERVICE_PORT}/`,
    "dbName": "GatewayGaiaX",
    "dbCollections": ["Configuration"],
    "defaultMongoDB": process.env.MONGO_DOCKER_URL || "mongodb://127.0.0.1:27017"
}

async function init() {
    try {
        client = new MongoClient((typeof process.env.MONGO_INITDB_ROOT_USERNAME != 'undefined') ? config.connectionString : config.defaultMongoDB,  { useNewUrlParser: true, useUnifiedTopology: true });
        await client.connect();
        const db = client.db(config.dbName);
        logger.info(`Connected to mongodb`);
        for (const collectionName of config.dbCollections) {
            const collections = await db.listCollections({ name: collectionName }).toArray();
            if (collections.length === 0) {
                await db.createCollection(collectionName);
                logger.info(`La collection ${collectionName} a été crée avec succès.`);
            }
        }
        await put('Configuration', { id: 'VCsConfiguration', object: defaultVCsConfiguration });
        await put('Configuration', { id: 'CredentialsSupported', object: defaultCredentialsSupported });
        await put('Configuration', { id: 'PresentationDefinitions', object: defaultPresentationDefinitions });
    } catch (error) {
        logger.error('Une erreur s\'est produite lors de la création de la collection:', error); 
        process.exit(1);
    }
}

async function put(collectionName, row) {
    await client.db(config.dbName).collection(collectionName).replaceOne({ "id" : row.id }, { "id" : row.id, "object": row.object }, { upsert: true });
}

async function getOne(collectionName, id) {
    const obj = await client.db(config.dbName).collection(collectionName).findOne({ "id" : id } );
    return (obj != null) ? obj.object : { message: `No object with id ${id} found in collection ${collectionName}` };
}

async function findMany(collectionName, request) {
    const collection = client.db(config.dbName).collection(collectionName);
    let f = request.hasOwnProperty("filter") ? request.filter : { $expr: { $gt: [ { $strLenCP: "id" }, 1] } }
    const result = await collection
        .find(f)
        .skip(request.hasOwnProperty("offset") ? parseInt(request.offset) || 0 : 0)
        .limit(request.hasOwnProperty("limit") ? parseInt(request.limit) || 10 : 10)
        .sort((request.hasOwnProperty("sort")) ? request.sort : { "ts": "1" }).toArray();
    return  { count: await collection.countDocuments(f), credentials: result.map(row => ({ "id": row.id, "ts" :row.ts, "status": row.status, "credential": row.credential }))};
}

module.exports = { init, put, getOne, findMany };