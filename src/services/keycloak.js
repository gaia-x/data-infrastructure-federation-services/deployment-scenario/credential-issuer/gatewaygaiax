const HttpError = require("../utils/httpError");

const keycloakConfig = {
    clientId: 'icp-portail',
    bearerOnly: false,
    serverUrl: 'https://keycloak.demo23.gxfs.fr',
    realm: 'icp',
};

function getKeycloakLoginUrl() {
    try {
        return `${keycloakConfig.serverUrl}/realms/${keycloakConfig.realm}/protocol/openid-connect/auth?client_id=${keycloakConfig.clientId}&response_type=code&scope=openid%20profile`;

    } catch (error) {
        throw new HttpError(`Error to get keycloak url: ${error.message}`, 500);
    }
}

module.exports = { getKeycloakLoginUrl };