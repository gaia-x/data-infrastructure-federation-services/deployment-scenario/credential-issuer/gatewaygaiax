const fs = require('fs').promises; 
const path = require('path');
const {getCompressedData} = require('../utils/utils');

exports.listCodeContries = async (req, res, next) => {
    try {
        const countriesJsonData = await fs.readFile(path.join(__dirname, '..', 'config', 'countries.json'), 'utf8');
        const responseData = {countries: JSON.parse(countriesJsonData)};
        const compressedData = await getCompressedData(JSON.stringify(responseData));
        res.setHeader('Content-Encoding', 'gzip');
        res.status(200).send(compressedData);
    } catch (error) {
        res.status(500).send('Erreur while getting list countries code');
    }
}

exports.listSubdivionsCountriesCode = async (req, res, next) => {
    try {
        const subdivionsJsonData = await fs.readFile(path.join(__dirname, '..', 'config', 'subdivisions-countries.json'), 'utf-8');
        const responseData = {subdivions: JSON.parse(subdivionsJsonData)};
        const compressedData = await getCompressedData(JSON.stringify(responseData));
        res.setHeader('Content-Encoding', 'gzip');
        res.status(200).send(compressedData);
    } catch (error) {
        res.status(500).send('Erreur while getting list subdivisions code');
    }
}

exports.listCodeContriesAndSundivions = async (req, res, next) => {
    try {
        res.setHeader('Content-Encoding', 'gzip');
        res.setHeader("Content-Type", "application/json");
        const countriesJsonData = await fs.readFile(path.join(__dirname, '..', 'config', 'countries.json'), 'utf8');
        const subdivionsJsonData = await fs.readFile(path.join(__dirname, '..', 'config', 'subdivisions-countries.json'), 'utf-8');
        const responseData = {
            countries: JSON.parse(countriesJsonData),
            subdivions: JSON.parse(subdivionsJsonData),
        };

        const compressedData = await getCompressedData(JSON.stringify(responseData));
        res.status(200).send(compressedData);
    } catch (error) {
        res.status(500).send('Erreur while getting list of subdivisions and countries code');
    }
}