const utils = require('../utils/utils');
const logger = require('../services/logger');
const plugin = require('../services/plugins');
const HttpError = require('../utils/httpError');
const { getOne } = require('../services/mongodb');

/**
 * Send the list of VC can be issued
 */
exports.listOfIssuableVCs = async (req, res, next) => {
    try {
        let response = [];
        const vcConfiguration = await getOne('Configuration', 'VCsConfiguration');
        for (const credential of vcConfiguration) {
            if (credential.issuer.some(element => new RegExp(element).test(req.headers.origin || 'localhost'))) response.push(credential);
        }
        logger.info(`The VC list was successfully retrieved`);
        res.status(200).json(response);
    }
    catch (error) {
        next(error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
}

exports.issuance = async (req, res, next) => {
    // Execute the plugin
    plugin.pluginsExecute(req.params.vcType, req.body.action, req.body, req)
        .then(async result => {
            // Send the conditions / informations for the issuance of the VC
            res.status(200).json(result);
            logger.info(`The ${req.body.action} for VC ${req.params.vcType} was successful`, { result: result });
        })
        .catch(error => {
            next(error instanceof HttpError ? error : new HttpError(error.message, 500));
        });
}