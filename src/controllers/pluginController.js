const fs = require('fs').promises; 
const path = require('path');
const {getCompressedData} = require('../utils/utils.js');

exports.pluginClientForm = async (req, res, next) => {
    try {
        res.setHeader("Content-Type", "text/javascript");
        res.setHeader('Content-Encoding', 'gzip');
        const filePath = path.join(__dirname, '..', 'plugins-client', 'legal-participant', 'legal-participant.js');
        const fileContents = await fs.readFile(filePath, 'utf-8');
        const compressedData = await getCompressedData(fileContents);
        res.status(200).send(compressedData);
    } catch (error) {
        res.status(500).send('Erreur while getting legal participant plugin');
    }
}

exports.pluginClientFormSelectCss = async (req, res, next) => {
    try {
        res.setHeader("Content-Type", "text/css");
        res.setHeader('Content-Encoding', 'gzip');
        
        // Adjust the file path based on your file structure
        const filePath = path.join(__dirname, '..', 'plugins-client', 'legal-participant', 'legal-participant-select.css');

        const fileContents = await fs.readFile(filePath, 'utf-8');
        const compressedData = await getCompressedData(fileContents);
        
        res.status(200).send(compressedData);
    } catch (error) {
        res.status(500).send('Error while getting legal participant CSS file');
    }
};

exports.pluginClientFormSelectJs = async (req, res, next) => {
    try {
        res.setHeader("Content-Type", "text/javascript");
        res.setHeader('Content-Encoding', 'gzip');
        const filePath = path.join(__dirname, '..', 'plugins-client', 'legal-participant', 'legal-participant-select.js');
        const fileContents = await fs.readFile(filePath, 'utf-8');
        const compressedData = await getCompressedData(fileContents);
        res.status(200).send(compressedData);
    } catch (error) {
        res.status(500).send('Erreur while getting legal participant Select JS file');
    }
}