const HttpError = require('../utils/httpError');
const { put, getOne } = require('../services/mongodb');

/* Get the openid-configuration file used for the issuance by the wallet */
exports.getCredentialsSupported = async (req, res, next) => {
    try {
        const configuration = await getOne('Configuration', 'CredentialsSupported');
        res.json(configuration);
    }
    catch (error) {
        next(new HttpError(`Error in getOIDCConfiguration: ${error.message}`, 500));
    }
}

/* Update the openid-configuration file used for the issuance by the wallet */
exports.putCredentialsSupported = async (req, res, next) => {
    try {
        await put('Configuration', { id: 'CredentialsSupported', object: req.body });
        res.json({ message: 'Configuration successfully updated' });
    }
    catch (error) {
        next(new HttpError(`Error in putOIDCConfiguration: ${error.message}`, 500));
    }
}

/* Update the all list of credentials that the gateway is able to issue */
exports.putCredentials = async (req, res, next) => {
    try {
        await put('Configuration', { id: 'VCsConfiguration', object: req.body });
        res.json({ message: 'Credentials successfully updated' });
    }
    catch (error) {
        next(new HttpError(`Error in putCredentials: ${error.message}`, 500));
    }
}

exports.getAllPresentationDefinitions = async (req, res, next) => {
    try {
        const result = await getOne('Configuration', 'PresentationDefinitions');
        res.json(result.map(item => item.name));
    }
    catch (error) {
        next(new HttpError(`Error in getPresentationDefinitions: ${error.message}`, 500));
    }
}


exports.getPresentationDefinition = async (req, res, next) => {
    try {
        const result = await getOne('Configuration', 'PresentationDefinitions');
        const item = result.find(item => item.name === req.params.name);
        if (item) return res.status(200).json(item.presentation);
        res.status(404).json({ error: 'Presentation definition not found' });
    }
    catch (error) {
        next(new HttpError(`Error in getPresentationDefinitions: ${error.message}`, 500));
    }
}