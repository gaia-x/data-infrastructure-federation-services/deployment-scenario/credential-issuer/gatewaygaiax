function loadScriptOrStyles(src, type, callback) {
    if (type === 'script') {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = src;
        script.onload = callback;
        document.head.appendChild(script);
    } else if (type === 'styles') {
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = src;
        link.onload = callback;
        document.head.appendChild(link);
    }
}

function createSelectOptions(options, selectElementId, selectElementToEdit) {
    document.getElementById(selectElementId).onchange = function() {
        selectElementToEdit.enable();
        const slectOptions = options.filter(obj => obj.code.startsWith(document.getElementById(selectElementId).value + "-"));
        selectElementToEdit.clearOptions();
        selectElementToEdit.addOptions(slectOptions)	
    };
}

async function createCountryCodeSelect(selectIds, codeCountries, codeSubContries) {
    let selectLegalSubdivisionsCode, selectHeadQuarterCountriesCode;
    selectIds.forEach((id) => {
        const inputElement = document.getElementById(id);
        const labelToRemove = document.getElementById(id + '-label');

        if (labelToRemove) {
            labelToRemove.remove();
        }

        if (!inputElement) {
            return;
        }

        const selectElement = document.createElement('select');
        selectElement.id = inputElement.id;
        selectElement.required = true;

        const placeholderOption = document.createElement('option');
        placeholderOption.value = '';
        placeholderOption.text = '';
        selectElement.add(placeholderOption);

        inputElement.replaceWith(selectElement);

        let isCountrySelect = id.includes('addressCountryCode');
        let placeholder = isCountrySelect ? 'Address Country Code *' : 'Country Subdivision Code *';
        let dataToSet = isCountrySelect ? codeCountries : [];

        let idToSelect = `#${id.replace(/:/g, '\\:')}`;
        let select = new window.TomSelect(idToSelect, {
            valueField: 'code',
            labelField: 'name',
            searchField: ['name', 'code'],
            options: dataToSet,
            placeholder
        });
        
        switch (id) {
            case 'root_gx:headquarterAddress_gx:addressCountryCode':
                createSelectOptions(codeSubContries, id, selectHeadQuarterCountriesCode);
                break;
            case 'root_gx:legalAddress_gx:addressCountryCode':
                createSelectOptions(codeSubContries, id, selectLegalSubdivisionsCode);
                break;
            case 'root_gx:headquarterAddress_gx:countrySubdivisionCode':
                selectHeadQuarterCountriesCode = select;
                select.disable();
                break;
            case 'root_gx:legalAddress_gx:countrySubdivisionCode':
                selectLegalSubdivisionsCode = select;
                select.disable();
                break;
        }
    });
}

function addAttributes(form) {
    form['gx:headquarterAddress']['gx:addressCountryCode'] = document.getElementById('root_gx:headquarterAddress_gx:addressCountryCode').value;
    form['gx:headquarterAddress']['gx:countrySubdivisionCode'] = document.getElementById('root_gx:headquarterAddress_gx:countrySubdivisionCode').value;
    form['gx:legalAddress']['gx:addressCountryCode'] = document.getElementById('root_gx:legalAddress_gx:addressCountryCode').value;
    form['gx:legalAddress']['gx:countrySubdivisionCode'] = document.getElementById('root_gx:legalAddress_gx:countrySubdivisionCode').value;
    return form;
}

fetch(window.countryCode).then((response) => {
    return response.json();
}).then((data) => {
    loadScriptOrStyles(window.pluginSelectCSS, 'styles', function () {
        loadScriptOrStyles(window.pluginSelectJS, 'script', function () {   
            let selectIds = ['root_gx:headquarterAddress_gx:countrySubdivisionCode', 'root_gx:legalAddress_gx:countrySubdivisionCode', 'root_gx:headquarterAddress_gx:addressCountryCode', 'root_gx:legalAddress_gx:addressCountryCode'];
            let countriesCode = JSON.parse(data.content).filter((item) => !item.hasOwnProperty('category') );
            let subdivisionsCode = JSON.parse(data.content).filter((item) => item.hasOwnProperty('category'));
            createCountryCodeSelect(selectIds, countriesCode, subdivisionsCode);
        });
    });
});

