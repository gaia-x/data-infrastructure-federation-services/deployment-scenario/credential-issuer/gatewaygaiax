FROM node:alpine

WORKDIR /usr/src/app

ENV PATH usr/src/app/node_modules/.bin:$PATH

COPY ./src ./src

COPY package*.json .

RUN npm install --silent

EXPOSE 3003
CMD ["npm", "start"]